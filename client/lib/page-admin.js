Meteor.subscribe('users')
Meteor.subscribe('startups')

var rolesList = {
  'startup-founder': 'Startup Founder',
  'investor': 'Investor',
  'admin': 'Administrator'
}


//
// MAIN
//
Template.pageAdmin.helpers({
  usersData: function() {
    var data = Meteor.users.find({}).fetch();
    console.log(data.length);
    data.forEach(function(dat) {
      for ( var i = 0; i < dat.roles.length; i++ )
        dat.roles[i] = rolesList[dat.roles[i]]
        // Get English name of role

      dat.roles = dat.roles.join(', ')
    });
    return data;
  },

  startup: function() {
    var startup = Startups.findOne({ userId: Meteor.userId() });
    return startup;
  },

  startups: function() {
    var startups = Startups.find({}).fetch();
    return startups;
  },

  url: function() {
    return window.location.href.slice(0, -5) + 'startup/'
  }
});


//
// ADD USER - FORM
//
Template.addUser.events({
  "submit .new-user": function(e) {
    e.preventDefault();
    var $form = $('.new-user')

    function err (err) {
      throw new Meteor.Error(err);
      $('.err').text(err)
      return false;
    }

    var user = {
      username: $form.find('[name="username"]').val().trim(),
      password: $form.find('[name="password"]').val(),
      profile: {
        name: $form.find('[name="name"]').val().trim() || $form.find('[name="username"]').val().trim()
      }
    }

    var role = $form.find('[name="role"]').val().trim()

    var regAlphanum = /^[a-zA-Z0-9_]*$/

    if ( !user.username ) return err("Username can't be empty")
    if ( !user.password ) return err("Password can't be empty")

    if ( 
      user.username.match(regAlphanum, 'g') &&
      rolesList[role]
    )
      Meteor.call("addUser", user, role)

    else
      err("Username can only be alphanumeric characters")

    return false;
  }
})


//
// ADD STARTUP - FORM
//
Template.addStartup.events({
  "submit .new-startup": function(e) {
    e.preventDefault();
    var $form = $('.new-startup')

    var startup = {
      name: $form.find('.name').val().trim(),
      desc: $form.find('.desc').val(),
      color: $form.find('.color').val().trim()
    }

    function err (err) {
      throw new Meteor.Error(err);
      $('.err').text(err)
      return false;
    }

    if ( !startup.name ) return err("Name can't be empty")

    Meteor.call('addStartup', startup)
    return false;
  }
})

//
// EDIT STARTUP - FORM
//
Template.editStartup.helpers({
  startup: function() {
    var startup = Startups.findOne({ userId: Meteor.userId() });
    return startup;
  },
  postMatches: function(p, post) {
    return p == post;
  }
})

Template.editStartup.events({
  "submit .edit-startup": function(e) {
    e.preventDefault()
    var $form = $(".edit-startup")
    var startup = {
      name: $form.find('.name').val().trim(),
      desc: $form.find('.desc').val().trim(),
      color: $form.find('.color').val().trim(),
      staff: [],
      products: [],
      income: parseFloat($form.find('[name="earning-income"]').val()) || 0,
      expenditures: parseFloat($form.find('[name="earning-expenditures"]').val()) || 0,
      funding: parseFloat($form.find('[name="earning-income"]').val()) || 0,
      salary:parseFloat($form.find('[name="earning-salary"]').val()) || 0,
      overhead: parseFloat($form.find('[name="earning-overhead"]').val()) || 0
    }

    $('.staff .staff__member').each(function() {
      var $staffMember = $(this)
      console.log($staffMember);
      startup.staff.push({
        name: $staffMember.find('.staff__member__name').val(),
        post: $staffMember.find('.staff__member__post').val()
      });
    });

    $('.products .products__item').each(function() {
      var $productItem = $(this)
      startup.products.push({
        name: $productItem.find('.products__item__name').val(),
        desc: $productItem.find('.products__item__desc').val()
      });
    });

    function err (err) {
      throw new Meteor.Error(err);
      $('.err').text(err)
      return false;
    }

    if ( !startup.name ) return err("Name can't be empty")
    if ( !startup.desc ) return err("Desc can't be empty")

    console.log(startup);

    Meteor.call('editStartup', startup)

    window.location.reload();
    return false;
  },

  "click .staff__add": function() {
    $('.staff').append('<div class="staff__member">'
      + '<input type="text" class="staff__member__name" placeholder="Name"/>'
      + '<select class="staff__member__post">'
      +   '<option value="Technical Manager">Technical Manager</option>'
      +   '<option value="Designer">Designer</option>'
      +   '<option value="Developer">Developer</option>'
      +   '<option value="Engineer">Engineer</option>'
      +   '<option value="Marketing Manager">Marketing Manager</option>'
      +   '<option value="PR">PR</option>'
      + '</select>'
      + '</div>')
  },

  "click .products__add": function() {
    $('.products').append('<div class="products__item">'
      + '<input type="text" class="products__item__name" placeholder="Name"/>'
      + '<input type="text" class="products__item__desc" class="input--large" placeholder="Description of the product"/>'
      + '</div>')
  }
})