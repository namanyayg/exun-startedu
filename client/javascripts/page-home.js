$(function() {
  $('.page__scroll-down').on('click', function(event) {
    event.preventDefault();
    $('html, body').animate({
      scrollTop: $(window).height()
    });
  });
});