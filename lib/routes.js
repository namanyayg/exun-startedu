Router.configure({
  layoutTemplate: 'layoutMain'
})

Router.route('/', function() {
  this.render('pageHome')
}, {
  onAfterAction: function() {
    document.title = "StartEdu"
  }
});

Router.route('/login', function() {
  if ( Meteor.userId() )
    this.redirect('/admin')
  else
    this.render('pageLogin')
}, {
  onBeforeAction: function() {
    $('body').addClass('body--sec')
    this.next();
  },
  onAfterAction: function() {
    document.title = "Login | StartEdu"
  }
})

Router.route('/admin', {
  onBeforeAction: function() {
    if ( Meteor.userId() ) {
      $('body').addClass('body--sec')
      this.layout('layoutAdmin')
      this.render('pageAdmin')
    }
    else {
      this.redirect('/')
    }
  },
  onAfterAction: function() {
    document.title = "Admin | StartEdu";
  }
});

Router.route('/startup', function() {
  this.redirect('/startups')
})

Router.route('/startups', function() {
  this.render('pageStartups')
}, {
  data: function() {
    return { startups: Startups.find({}).fetch() };
  },
  onAfterAction: function() {
    document.title = "Startups | StartEdu"
  }
})

Router.route('/startup/:startupName', function() {
  var startupName = this.params.startupName;
  var startup = Startups.findOne({ slug: startupName });

  if ( startup )
    return this.render('startup', { data: startup })

  return this.render('404')
}, {
  onAfterAction: function() {
    document.title = this.params.startupName + " | Startups - StartEdu"
  }
});

Router.route('/logout', function() {
  Meteor.logout();
  return this.redirect('/')
})
