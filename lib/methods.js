Meteor.methods({
  //
  // ADD USER
  //
  addUser: function(user, role) {
    if ( !(
      Meteor.userId() && 
      Roles.userIsInRole( Meteor.userId(), ['admin'] )
    )) {
      throw new Meteor.Error("not-authorized")
    }

    var regAlphanum = /^[a-zA-Z0-9_]*$/

    if ( 
      user.username.match(regAlphanum, 'g') &&
      role.indexOf(['admin', 'startup-founder', 'investor'])
    ) {
      var newUser = Accounts.createUser(user)

      if ( Meteor.isServer )
        Roles.addUsersToRoles(newUser, role)
    }
    else {
      throw new Meteor.Error("invalid-form")
    }
  },

  //
  // ADD STARTUP
  //
  addStartup: function(startup) {
    if ( !Meteor.userId() 
      || !Roles.userIsInRole(Meteor.userId(), ['startup-founder'])
    ) throw new Meteor.Error("not-authorized")
    if ( !startup.name ) throw new Meteor.Error("empty-name")

    startup['userId'] = Meteor.userId();
    startup['slug'] = startup.name.replace(/\s/g, '')
    Startups.insert(startup)
  },

  //
  // EDIT STARTUP
  //
  editStartup: function(startup) {
    if ( !Meteor.userId() 
      || !Roles.userIsInRole(Meteor.userId(), ['startup-founder'])
    ) throw new Meteor.Error("not-authorized")

    Startups.update(
      { userId: Meteor.userId() },
      {
        $set: startup
      }
    );

  }
});