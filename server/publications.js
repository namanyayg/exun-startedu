Meteor.publish('users', function() {
  return Meteor.users.find({}, { profile: 1, role: 1, username: 1 });
});

Meteor.publish('startups', function() {
  return Startups.find({})
});